package com.example.truongtankhanh.myapplication.Model;

public class User {

    private String userName, photoUri;

    public User() {
    }

    public User(String userName, String photoUri) {
        this.setUserName(userName);
        this.setPhotoUri(photoUri);
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPhotoUri() {
        return photoUri;
    }

    public void setPhotoUri(String photoUri) {
        this.photoUri = photoUri;
    }
}

package com.example.truongtankhanh.myapplication.Model;

public class UserInfo extends User {

    private String email;

    public UserInfo(String userName, String photoUri, String email) {
        super(userName, photoUri);
        this.setEmail(email);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

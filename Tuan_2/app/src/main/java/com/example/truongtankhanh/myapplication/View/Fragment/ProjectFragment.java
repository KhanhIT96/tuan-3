package com.example.truongtankhanh.myapplication.View.Fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.truongtankhanh.myapplication.Model.Project;
import com.example.truongtankhanh.myapplication.Model.ProjectRecyclerViewAdapter;
import com.example.truongtankhanh.myapplication.Model.SocialProject;
import com.example.truongtankhanh.myapplication.R;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProjectFragment extends Fragment {

    private RecyclerView recyclerView;
    private List<Project> projectList = new ArrayList<>();
    private ProjectRecyclerViewAdapter adapter;

    private DatabaseReference reference;

    public ProjectFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_project, container, false);

        this.recyclerView = view.findViewById(R.id.recyclerView);

        reference = FirebaseDatabase.getInstance().getReference("NhaDat");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                projectList.clear();

                for (DataSnapshot data : dataSnapshot.child("DuAn").child("NhaOXaHoi").getChildren()) {
                    Gson gson = new Gson();
                    String json = gson.toJson(data.getValue());
                    SocialProject project = gson.fromJson(json, SocialProject.class);
                    projectList.add(project);
                }

                Toast.makeText(getContext(),projectList.size() + "",Toast.LENGTH_SHORT).show();
                adapter = new ProjectRecyclerViewAdapter(getActivity(),projectList);
                recyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
                recyclerView.setAdapter(adapter);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        //getAllProject();

        return view;
    }

    private void getAllProject() {

    }

}

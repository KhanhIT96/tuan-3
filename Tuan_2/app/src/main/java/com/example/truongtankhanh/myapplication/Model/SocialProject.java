package com.example.truongtankhanh.myapplication.Model;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SocialProject extends Project {

    public static FirebaseDatabase database;
    public static DatabaseReference myDataRefer;

    public SocialProject(String projectID, String projectName, String projectTitle, String projectAddress, String projectPrice, String projectArea, String projectImg) {
        super(projectID, projectName, projectTitle, projectAddress, projectPrice, projectArea, projectImg);
    }

    public static void Init() {
        database = FirebaseDatabase.getInstance();
        myDataRefer = database.getReference("NhaDat");
    }

    public static void NhanData(final String maDuAn) {
        Init();
    }
}

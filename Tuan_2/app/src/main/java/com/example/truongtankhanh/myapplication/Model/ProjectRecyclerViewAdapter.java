package com.example.truongtankhanh.myapplication.Model;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.truongtankhanh.myapplication.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ProjectRecyclerViewAdapter extends RecyclerView.Adapter<ProjectRecyclerViewAdapter.ProjectHolder> {

    private Context context;
    private List<Project> projectList;

    public ProjectRecyclerViewAdapter(Context context, List<Project> projectList) {
        this.context = context;
        this.projectList = projectList;
    }

    @NonNull
    @Override
    public ProjectHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view;
        view = LayoutInflater.from(context).inflate(R.layout.item_cardview,viewGroup,false);
        return new ProjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProjectHolder projectHolder, int i) {
        projectHolder.txt_name_project.setText(this.projectList.get(i).getTenDuAn());
        projectHolder.txt_address_project.setText(this.projectList.get(i).getDiaChi());
        Picasso.get().load(this.projectList.get(i).getUrl())
                .into(projectHolder.img_project);
    }

    @Override
    public int getItemCount() {
        return this.projectList.size();
    }

    static class ProjectHolder extends RecyclerView.ViewHolder{

        ImageView img_project;
        TextView txt_name_project, txt_address_project;
        CardView cardview_project;

        public ProjectHolder(@NonNull View itemView) {
            super(itemView);

            img_project         = itemView.findViewById(R.id.img_project);
            txt_name_project    = itemView.findViewById(R.id.txt_name_project);
            txt_address_project = itemView.findViewById(R.id.txt_address_project);
            cardview_project    = itemView.findViewById(R.id.cardview_project);
        }
    }
}

package com.example.truongtankhanh.myapplication.Model;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.truongtankhanh.myapplication.View.Fragment.ProjectFragment;
import com.example.truongtankhanh.myapplication.View.Fragment.TwoFragment;

public class PagerAdapter extends FragmentStatePagerAdapter {


    public PagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        Fragment frag=null;
        switch (i){
            case 0:
                frag = new ProjectFragment();
                break;
            case 1:
                frag = new TwoFragment();
                break;
//            case 2:
//                frag = new ProjectFragment();
//                break;
        }
        return frag;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        String title = "";
        switch (position){
            case 0:
                title = "Dự án";
                break;
            case 1:
                title = "Tính năng";
                break;
//            case 2:
//                title = "Dự án";
//                break;
        }
        return title;
    }
}

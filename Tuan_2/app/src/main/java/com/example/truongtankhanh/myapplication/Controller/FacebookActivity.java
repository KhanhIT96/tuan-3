package com.example.truongtankhanh.myapplication.Controller;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.truongtankhanh.myapplication.Model.UserInfo;
import com.example.truongtankhanh.myapplication.R;
import com.example.truongtankhanh.myapplication.View.HomeActivity;
import com.example.truongtankhanh.myapplication.View.RoundedTransformation;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Picasso;

public class FacebookActivity extends AppCompatActivity {

    private Button btnSignUp;
    private TextView txtUserName, txtEmail;
    private ImageView imgUser;

    private LoginButton btnLogin;
    private CallbackManager callbackManager;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facebook);

        init();

        this.btnLogin.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });

        this.btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signOut();
                finish();
            }
        });
    }

    private void init() {
        this.btnSignUp      = findViewById(R.id.btnSignUp);
        this.txtUserName    = findViewById(R.id.txtUserName);
        this.txtEmail       = findViewById(R.id.txtEmail);
        this.imgUser        = findViewById(R.id.imgUser);
        this.btnLogin       = findViewById(R.id.btnLogin);
        this.mAuth          = FirebaseAuth.getInstance();
        this.btnSignUp.setVisibility(View.GONE);

        this.callbackManager = CallbackManager.Factory.create();
        this.btnLogin.setReadPermissions("email", "public_profile");
    }

    private void signOut() {
        // Firebase sign out
        this.mAuth.signOut();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        callbackManager.onActivityResult(requestCode,resultCode,data);
    }

    private void handleFacebookAccessToken(AccessToken token) {
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            btnSignUp.setVisibility(View.VISIBLE);
                            FirebaseUser user = mAuth.getCurrentUser();
                            assert user != null;
                            UserInfo userInfo = new UserInfo(user.getDisplayName(),user.getPhotoUrl().toString(),user.getEmail());
                            txtUserName.setText(userInfo.getUserName());
                            txtEmail.setText(userInfo.getEmail());
                            Picasso.get().load(userInfo.getPhotoUri())
                                    .transform(new RoundedTransformation(60,0))
                                    .into(imgUser);
                            startActivity(new Intent(getApplicationContext(),HomeActivity.class));
                        } else {
                            Toast.makeText(getApplicationContext(), "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
